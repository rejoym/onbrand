<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_client extends Model
{
    use HasFactory;
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'username', 'email', 'address', 'phone_no', 'city', 'country', 'user_id'];
    /**
     * reverse one to many relation with the user model
     * @return instance of the relation
     */
    public function user() {
      return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
