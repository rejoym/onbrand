<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Http\Request;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    /**
     * Get the password for the user
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }
    /**
     * defines a one to many relation product model
     * @return instance of the relation
     */
    public function products() {
        return $this->hasMany('App\Models\Product');
    }
    /**
     * defines a one to many relation with client model
     * @return [type] [description]
     */
    public function clients() {
        return $this->hasMany('App\Models\Client');
    }
    /**
     * Create user if not empty
     * @param  Request $request [description]
     * @return object
     */
    public static function create(Request $request)
    {
        $user = new User();
        if(!empty($request->get('username')))
        {
            $user->username = $request->get('username');
        }
        if(!empty($request->get('password')))
        {
            $user->password = bcrypt($request->get('password'));
        }
        //save user details
        $user->save();
        //return user
        return $user;
    }
}
