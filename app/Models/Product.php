<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Product extends Model
{
    use HasFactory;
    /**
     * arrays that are mass assignable
     * @var instance variable
     */
    protected $fillable = ['product_title', 'product_description', 'remarks', 'price', 'isApproved', 'path', 'file_name', 'user_id'];
    /**
     * function to define an inverse one to many relationship
     * @return relation
     */
    public function user() {
      return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    /**
     * Create a new product without setting the status of approval which is by default unapproved
     *
     * @return product
     */
    public static function create(Request $request) {
      $product = new Product();

      $product->product_title = $request->get('product_title');
      $product->product_description = $request->get('product_description');
      $product->remarks = $request->get('remarks');
      $product->price = $request->get('price');
      $product->path = $request->get('path');
      $product->file_name = $request->get('file_name');
      $product->user_id = $request->User()->id;

      $product->save();

      return $product;
    }
    /**
     * Update / Edit product detail without the option to edit user detail
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public static function update(Request $request) {
      $product = Product::where('id', $request->get('id'))->first();

      $product->product_title = $request->get('product_title');
      $product->product_description = $request->get('product_description');
      $product->remarks = $request->get('remarks');
      $product->price = $request->get('price');
      $product->path = $request->get('path');
      $product->file_name = $request->get('file_name');

      $product->save();
      return $product;
    }
    /**
     * Delete a product by id
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public static function delete(Request $request) {
      Product::where('id', $request->get('id'))->delete();
    }
    /**
     * Returns all products created by particular user
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function readAllProductCreatedByUser(Request $request) {
      return Product::where('user_id', $request->User()->id)->get();
    }
}
