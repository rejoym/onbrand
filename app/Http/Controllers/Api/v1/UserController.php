<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
  /**
   * Login user and return a token
   * @param  Request $request [description]
   * @return JsonResponse
   */
  public function login(Request $request)
  {
    $token = $this->guard($request->username, $request->password);

    if($token)
    {
      return ResponseHelper::jsonResponse(null, Response::HTTP_OK, config('message.success'))->header('Authorization', $token)->send();
    }
    else
    {
      return ResponseHelper::jsonResponse(null, Response::HTTP_BAD_REQUEST, config('messages.fail'))->send();
    }
  }
}
