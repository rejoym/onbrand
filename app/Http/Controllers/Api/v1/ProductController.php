<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Helpers\InputHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\ResponseHelper;


class ProductController extends Controller
{
    /**
     * Function to return all the products
     *
     * @return array
     */
    public function readProduct()
    {
      InputHelper::inputChecker($request, null,
        function(Request $request) {
          Product::readAll($request);
          return ResponseHelper::jsonResponse(null, Response::HTTP_OK, config('message.success'))->send();
      });
    }
    /**
     * create new product
     * @param  Request $request [description]
     * @return JsonRepsonse
     */
    public function createProduct(Request $request) {
      /**
       * helper function to create user input
       * @param $request
       * @param items to be inserted from the $request
       * @param response
       */
      InputHelper::inputChecker($request, [
        $request->product_title,
        $request->product_description,
        $request->remarks,
        $request->price,
        $request->path,
        $request->file_name
      ], function(Request $request) {
        Product::create($request);
        return ResponseHelper::jsonResponse(null, Response::HTTP_OK, config('message.success'))->send();
      });
    }
    /**
     * update the product
     * @param  Request $request [description]
     * @return JsonResponse
     */
    public function updateProduct(Request $request) {
      /**
       * helper function to create user input
       * @param $request
       * @param items to be inserted from the $request
       * @param response
       */
      InputHelper::inputChecker($request, [
        $request->id,
        $request->product_title,
        $request->product_description,
        $request->remarks,
        $request->price,
        $request->path,
        $request->file_name
      ], function(Request $request) {
        Product::updateOne($request);
        return ResponseHelper::jsonResponse(null, Response::HTTP_OK, config('message.success'))->send();
      });
    }

    public function deleteProduct(Request $request) {
      /**
       * helper function to create user input
       * @param $request
       * @param items to be inserted from the $request
       * @param response
       */
      InputHelper::inputChecker($request, [
        $request->id
      ], function(Request $request) {
        Product::deleteOne($request);
        return ResponseHelper::jsonResponse(null, Response::HTTP_OK, config('message.success'))->send();
      });
    }
}
