<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function(){

  Route::group(['prefix' => 'product'], function(){
    Route::get('all', 'App\Http\Controllers\Api\v1\ProductController@getAll');
    Route::post('/create', 'App\Http\Controllers\Api\v1\ProductController@createProduct');
    Route::post('/update', 'App\Http\Controllers\Api\v1\ProductController@updateProduct');
    Route::post('/delete', 'App\Http\Controllers\Api\v1\ProductController@deleteProduct');
  });
// });